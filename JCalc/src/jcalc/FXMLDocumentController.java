/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcalc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.BiFunction;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;


/**
 *
 * @author student
 */
public class FXMLDocumentController implements Initializable
{
    @FXML
    private TextArea txtDisplay;

    private Boolean equalState;
    private final Entry entry1 = new Entry();
    private final Entry entry2 = new Entry();
    private Entry focus;
    private BiFunction<Entry, Entry, Double> lastOp;
    private final Map<String, BiFunction<Entry, Entry, Double>> ops = new HashMap<>();

    public static final String CLEAR_TEXT = "0.0";


    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        ops.put("+", (a, b) -> a.asNum() + b.asNum());
        ops.put("-", (a, b) -> a.asNum() - b.asNum());
        ops.put("*", (a, b) -> a.asNum() * b.asNum());
        ops.put("/", (a, b) -> a.asNum() / b.asNum());
        resetState();
    }


    /**
     * Handles number presses
     *
     * @param event
     */
    @FXML
    private void handleButtonNum(ActionEvent event)
    {
        if (equalState)
        {
            resetState();
            equalState = false;
        }

        String num = ((Button)event.getSource()).getText();
        focus.addNum(num);
        txtDisplay.setText(focus.toString());
    }


    /**
     * Handles operation buttons +, -, * and /
     *
     * @param event
     */
    @FXML
    private void handleButtonOp(ActionEvent event)
    {
        String opStr = ((Button)event.getSource()).getText();

        if (lastOp == null)
            focus = entry2;
        else
        {
            entry1.op(focus, lastOp);
            focus.clearEntry();
            txtDisplay.setText(entry1.toString());
        }

        lastOp = ops.get(opStr);
    }


    /**
     * Handles clearing the calculator memory
     *
     * @param event
     */
    @FXML
    private void handleButtonClear(ActionEvent event)
    {
        txtDisplay.setText(CLEAR_TEXT);
        resetState();
    }


    /**
     * Handles the equals button
     *
     * @param event
     */
    @FXML
    private void handleButtonEqual(ActionEvent event)
    {
        equalState = true;
        entry1.op(entry2, lastOp);
        entry2.clearEntry();
        txtDisplay.setText(entry1.toString());
    }


    /**
     * Resets the state of the calculator
     */
    private void resetState()
    {
        entry1.clearEntry();
        entry2.clearEntry();
        focus = entry1;
        lastOp = null;
        equalState = false;
    }
}
