/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcalc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;


/**
 * Entry class for storing inputs
 *
 * @author student
 */
public class Entry
{
    private Boolean decimal = false;
    private List<String> input = new ArrayList<>();


    /**
     * Appends a button press to the internal list input
     *
     * @param n the number pressed
     */
    public void addNum(String n)
    {
        if (!n.equals("."))
            input.add(n);
        else
        {
            // Don't accept another period 
            if (decimal)
                return;

            // If '.' is pressed first, append '0' to the num list first
            if (input.isEmpty())
                input.add("0");

            input.add(n);
            decimal = true;
        }
    }


    /**
     * Applies an operation to another entry to this entry
     *
     * @param e    Another instance of entry
     * @param func An anonymous function to operate on the entry
     */
    public void op(Entry e, BiFunction<Entry, Entry, Double> func)
    {
        input = Arrays.asList(Double.toString(func.apply(this, e)).split(""));
    }


    /**
     * Clears the input list
     */
    public void clearEntry()
    {
        input = new ArrayList<>();
    }


    /**
     * Converts the input list to a double
     *
     * @return the double value of input
     */
    public double asNum()
    {
        return Double.parseDouble(this.toString());
    }


    /**
     * Returns the string representation of the input
     *
     * @return Object string
     */
    @Override
    public String toString()
    {
        return input.isEmpty() ? "0.0" : String.join("", input);
    }
}
